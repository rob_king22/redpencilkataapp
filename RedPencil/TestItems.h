//
//  TestItems.h
//  RedPencil
//
//  Created by Robert King on 3/10/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PromotionItem.h"

@interface TestItems : NSObject

+ (NSArray *)testItems;

@end
