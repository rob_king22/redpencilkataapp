//
//  Item.h
//  RedPencil
//
//  Created by Robert King on 3/9/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Item : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic) CGFloat originalPrice;
@property (nonatomic) CGFloat activePrice;


- (id)initWithName:(NSString *)name andPrice:(CGFloat)price;

@end
