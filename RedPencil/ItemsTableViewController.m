//
//  ItemsTableViewController.m
//  RedPencil
//
//  Created by Robert King on 2/25/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import "ItemsTableViewController.h"

@interface ItemsTableViewController ()

@property (nonatomic, strong) NSArray *itemsArray;

@end

@implementation ItemsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [TestItems testItems].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [cell setCellItem:[[TestItems testItems] objectAtIndex:indexPath.row]];
    
    return cell;
}

@end
