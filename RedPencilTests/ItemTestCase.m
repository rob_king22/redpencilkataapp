//
//  ItemTestCase.m
//  RedPencil
//
//  Created by Robert King on 3/9/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Item.h"
#import "TestItems.h"

@interface ItemTestCase : XCTestCase
@end

@implementation ItemTestCase

- (void)setUp {
    [super setUp];
    
}

- (void)tearDown {
    [super tearDown];
}


- (void)testThatInitWithNameAndPriceReturnsAnItem {
    Item *testItem = [[Item alloc] initWithName:nil andPrice:0.0];
    XCTAssertNotNil(testItem, @"testItem is not nil");
}

- (void)testThatNameAndPriceAreEqualToInputs {
    Item *testItem = [[Item alloc] initWithName:@"Item 1" andPrice:0.0];
    XCTAssertEqual(testItem.name, @"Item 1", @"Both should equal Item 1");
    XCTAssertEqual(testItem.originalPrice, 0.0, @"Both should equal 0.0");
}

@end
