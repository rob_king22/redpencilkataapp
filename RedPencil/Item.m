//
//  Item.m
//  RedPencil
//
//  Created by Robert King on 3/9/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import "Item.h"

@implementation Item

- (id)initWithName:(NSString *)name andPrice:(CGFloat)price
{
    self = [super init];
    if (self) {
        self.name = name;
        self.originalPrice = price;
    }
    return self;
}

@end
