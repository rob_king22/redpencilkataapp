//
//  PromotionItem.m
//  RedPencil
//
//  Created by Robert King on 3/31/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import "PromotionItem.h"

@implementation PromotionItem

- (id)initWithName:(NSString *)name andPrice:(CGFloat)price andPromotionPrice:(CGFloat)activePrice
{
    self = [super init];
    if (self) {
        self.name = name;
        self.originalPrice = price;
        [self changePrice:activePrice];
    }
    return self;
}

- (void)changePrice:(CGFloat)newPrice
{
    [self checkForPromotion:newPrice];
    self.activePrice = newPrice;
}

- (void)checkForPromotion:(CGFloat)newPrice
{
    if ([self isPromotionPrice:newPrice]) {
        if ([self isDatePromotionCompatible] && !self.isPromotionOngoing) {
            [self startPromotion];
        } else if (![self isDatePromotionCompatible]) {
            [self endPromotion];
        } else {
            //promotion status remains unchanged
        }
    }
    else {
        if (self.isPromotionOngoing) {
            [self endPromotion];
        }
    }
}

- (BOOL)isPromotionPrice:(CGFloat)newPrice
{
    if (self.activePrice < newPrice && self.isPromotionOngoing) {
        return false;
    } else if (newPrice <= (self.originalPrice * 0.95) && newPrice >= (self.originalPrice * 0.70)) {
        return true;
    } else {
        return false;
    }
}

- (BOOL)isDatePromotionCompatible
{
    if (self.promotionStartDate == nil && self.promotionEndDate == nil) {
        return true; //If both date values are nil, then a promotion has never occured
    } else if ([[NSDate date] compare:self.promotionEndDate]==NSOrderedDescending) {
        return true; //If the date occurs before the promotion end date, then the current promotion may continue
    } else {
        NSDate *endPlusThirtyDate;
        NSDateComponents *endPlusThrityDateComponents = [NSDateComponents new];
        NSCalendar *dateCalendar = [NSCalendar currentCalendar];
        endPlusThrityDateComponents.day = 30;
        endPlusThirtyDate = [dateCalendar dateByAddingComponents:endPlusThrityDateComponents toDate:self.promotionEndDate options:0];
        //If the stable period has elapsed succesfully, then a new promotion is permitted
        if ([[NSDate date] compare:endPlusThirtyDate] == NSOrderedDescending) {
            return true;
        } else {
            return false;
        }
    }
}

- (void)startPromotion
{
    NSDateComponents *dayComponent = [NSDateComponents new];
    NSCalendar *dateCalendar = [NSCalendar currentCalendar];
    dayComponent.day = 30;
    self.promotionStartDate = [NSDate date];
    self.promotionEndDate = [dateCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    self.isPromotionOngoing = true;
}

- (void)endPromotion
{
    self.promotionStartDate = nil;
    self.isPromotionOngoing = false;
}

@end
