//
//  ItemTableViewCell.m
//  RedPencil
//
//  Created by Robert King on 3/10/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import "ItemTableViewCell.h"

@interface ItemTableViewCell()

@property (nonatomic, strong) PromotionItem *item;

@end

@implementation ItemTableViewCell

- (void)setCellItem:(PromotionItem *)item
{
    self.item = item;
    self.nameLabel.text = item.name;
    if (!item.activePrice) {
        self.priceLabel.text = [NSString stringWithFormat:@"Price: $%.2f", item.originalPrice];
    }
    else {
        NSMutableAttributedString *originalPriceAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"$%.2f", item.originalPrice]];
        NSMutableAttributedString *priceString = [[NSMutableAttributedString alloc] initWithString:@"Price: "];
        NSString *promotionPrice = [NSString stringWithFormat:@" %@", [NSString stringWithFormat:@"$%.2f", item.activePrice]];
        [originalPriceAttributedString addAttributes:@{NSStrikethroughStyleAttributeName: @(NSUnderlineStyleSingle)
                                                       , NSStrikethroughColorAttributeName: [UIColor redColor]} range:NSMakeRange(0, [originalPriceAttributedString length])];
        [priceString appendAttributedString:originalPriceAttributedString];
        
        
        self.priceLabel.attributedText = priceString;
        self.promoPriceLabel.text = promotionPrice;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
