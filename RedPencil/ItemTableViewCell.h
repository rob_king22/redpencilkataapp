//
//  ItemTableViewCell.h
//  RedPencil
//
//  Created by Robert King on 3/10/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PromotionItem.h"

@interface ItemTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *promoPriceLabel;

- (void)setCellItem:(Item *)item;

@end
