//
//  AppDelegate.h
//  RedPencil
//
//  Created by Robert King on 12/13/14.
//  Copyright (c) 2014 ROB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

