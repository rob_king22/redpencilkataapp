//
//  main.m
//  RedPencil
//
//  Created by Robert King on 12/13/14.
//  Copyright (c) 2014 ROB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
