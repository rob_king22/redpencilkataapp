//
//  PromotionItemTestCase.m
//  RedPencil
//
//  Created by Robert King on 3/31/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Item.h"
#import "PromotionItem.h"

@interface PromotionItemTestCase : XCTestCase

@end

@implementation PromotionItemTestCase

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testThatPromotionItemIsSubclassOfItem {
    XCTAssertTrue([PromotionItem isSubclassOfClass:[Item class]], @"PromotionItem should subclass Item");
}

- (void)testThatInitWithNameAndPriceAndPromotionPriceReturnsAPromotionItem {
    PromotionItem *testItem = [[PromotionItem alloc] initWithName:@"Item" andPrice:0.0 andPromotionPrice:0.0];
    XCTAssertNotNil(testItem, @"testItem should not be nil");
}

- (void)testThatInitWithNameAndPriceAndPromotionPriceValuesEqualInputs {
    PromotionItem *testItem = [[PromotionItem alloc] initWithName:@"Item" andPrice:1.00 andPromotionPrice:0.75];
    XCTAssertEqual(testItem.name, @"Item", @"Both should equal Item 2");
    XCTAssertEqual(testItem.originalPrice, 1.00, @"Both should equal 1.00");
    XCTAssertEqual(testItem.activePrice, 0.75, @"Both should equal 0.75");
}

- (void)testChangePriceCorrectlyChangesThePrice {
     PromotionItem *testItem = [[PromotionItem alloc] initWithName:@"Item" andPrice:1.00 andPromotionPrice:0.75];
    [testItem changePrice:0.70];
    XCTAssertEqual(testItem.activePrice, 0.70, @"The price should have changed from .75 to .70");
}

- (void)testFirstPriceChangeWithPromoPriceStartsAPromotion {
    PromotionItem *testItem = [[PromotionItem alloc] initWithName:@"Item " andPrice:1.00];
    [testItem changePrice:0.75];
    XCTAssertTrue(testItem.isPromotionOngoing, @"Price change should trigger a Red Pencil Promotion");
}

- (void)testFirstPriceChangeWithNonPromoPriceDoesNotStartAPromo {
    PromotionItem *testItem = [[PromotionItem alloc] initWithName:@"Item 1" andPrice:1.00];
    [testItem changePrice:0.96];
    XCTAssertFalse(testItem.isPromotionOngoing, @"Price change should not trigger a Red Pencil Promotion");
}

- (void)testThatPriceChangeHigherOrLowerThanThresholdDoesNotStartPromotion {
    PromotionItem *testItem = [[PromotionItem alloc] initWithName:@"Item" andPrice:1.00];
    [testItem changePrice:0.59];
    XCTAssertFalse(testItem.isPromotionOngoing, @"Price change should not trigger a Red Pencil Promotion");
    [testItem changePrice:1.25];
    XCTAssertFalse(testItem.isPromotionOngoing, @"Price change should not trigger a Red Pencil Promotion");
}

- (void)testThatIfPriceIncreasesDuringPromoThePromotionEnds {
    PromotionItem *testItem = [[PromotionItem alloc] initWithName:@"Item" andPrice:1.00];
    [testItem changePrice:0.75];
    [testItem changePrice:0.78];
    XCTAssertFalse(testItem.isPromotionOngoing, @"Increasing the price during a promotion should immeadiately end the promotion");
}

- (void)testThatWhenAPromotionEndsAnotherCannotStartForThirtyDays {
    PromotionItem *testItem = [[PromotionItem alloc] initWithName:@"Item" andPrice:1.00];
    [testItem changePrice:0.75];
    [testItem changePrice:0.76]; //This should end the promotion we started
    [testItem changePrice:0.74]; //This should not begin a new promotion
    XCTAssertFalse(testItem.isPromotionOngoing, @"Changing the price within 30 days of a promion end should not start another promotion");
    
    NSDateComponents *fifteenDaysInFutureComponents = [NSDateComponents new];
    NSCalendar *dateCalendar = [NSCalendar currentCalendar];
    fifteenDaysInFutureComponents.day = -15;
    testItem.promotionEndDate = [dateCalendar dateByAddingComponents:fifteenDaysInFutureComponents toDate:testItem.promotionEndDate options:0];
    [testItem changePrice:0.74]; //This should also not begin a new promotion
    XCTAssertFalse(testItem.isPromotionOngoing, @"Changing the price within 30 days of a promion end should not start another promotion");
}

- (void)testThatChangingThePriceAfterStableDurationDoesStartPromotion {
    PromotionItem *testItem = [[PromotionItem alloc] initWithName:@"Item" andPrice:1.00];
    [testItem changePrice:0.75];
    [testItem changePrice:0.77]; //should immediatly end promotion
    
    NSDateComponents *subtractDayComponent = [NSDateComponents new];
    NSCalendar *dateCalendar = [NSCalendar currentCalendar];
    subtractDayComponent.day = -30;
    testItem.promotionEndDate = [dateCalendar dateByAddingComponents:subtractDayComponent toDate:testItem.promotionEndDate options:0]; //This should simulate 30 days passing
    [testItem changePrice:0.74];
    XCTAssertTrue(testItem.isPromotionOngoing, @"Changing the price now should start a new promotion");
}

@end
