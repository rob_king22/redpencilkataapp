//
//  TestItems.m
//  RedPencil
//
//  Created by Robert King on 3/10/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import "TestItems.h"

@implementation TestItems

+ (NSArray *)testItems
{
    PromotionItem *item1 = [[PromotionItem alloc] initWithName:@"Bacon" andPrice:3.50];
    PromotionItem *item2 = [[PromotionItem alloc] initWithName:@"Beans" andPrice:2.00 andPromotionPrice:1.15];
    PromotionItem *item3 = [[PromotionItem alloc] initWithName:@"Butter" andPrice:1.50];
    PromotionItem *item4 = [[PromotionItem alloc] initWithName:@"Poptarts" andPrice:3.00 andPromotionPrice:2.50];
    NSArray *itemArray = @[item1, item2, item3, item4];
    return itemArray;
}

@end
