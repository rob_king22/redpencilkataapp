//
//  PromotionItem.h
//  RedPencil
//
//  Created by Robert King on 3/31/15.
//  Copyright (c) 2015 ROB. All rights reserved.
//

#import "Item.h"

@interface PromotionItem : Item

@property (nonatomic, strong) NSDate *promotionStartDate;
@property (nonatomic, strong) NSDate *promotionEndDate;
@property (nonatomic) BOOL isPromotionOngoing;

- (id)initWithName:(NSString *)name andPrice:(CGFloat)price andPromotionPrice:(CGFloat)activePrice;
- (void)changePrice:(CGFloat)newPrice;

@end
